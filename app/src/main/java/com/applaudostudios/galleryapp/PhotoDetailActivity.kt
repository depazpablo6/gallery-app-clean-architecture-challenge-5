package com.applaudostudios.galleryapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.galleryapp.app.GalleryApplication
import com.applaudostudios.galleryapp.databinding.ActivityPhotoDetailBinding
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModelFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class PhotoDetailActivity : AppCompatActivity() {

    companion object {
        const val PHOTO_ID = "PHOTO_ID"
    }

    private lateinit var binding: ActivityPhotoDetailBinding

    private val photoViewModel: PhotoViewModel by viewModels {
        PhotoViewModelFactory((application as GalleryApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotoDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val photoId = intent.getIntExtra(PHOTO_ID, 0)
        photoViewModel.getPhoto(photoId)
        photoViewModel.photo.observe(this, {
            binding.idPhoto.text = it.uid.toString()
            binding.albumPhoto.text = it.albumId.toString()
            binding.titlePhoto.text = it.title
            binding.urlPhoto.text = it.url
            val url = it.url.replace("https", "http")
            setPhoto(url)
        })

    }

    private fun setPhoto(photoUrl: String) {

        val url = GlideUrl(
            photoUrl,
            LazyHeaders.Builder()
                .addHeader("User-Agent", "your-user-agent")
                .build()
        )

        Glide.with(binding.root)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .fitCenter()
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.ic_launcher_background)
            .into(binding.imageView)
    }
}