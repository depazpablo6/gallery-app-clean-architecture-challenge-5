package com.applaudostudios.galleryapp.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.galleryapp.model.Photo
import kotlinx.coroutines.flow.Flow

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(photos: List<Photo>)

    @Query("DELETE FROM Photo")
    fun deleteAll()

    @Query("SELECT * FROM Photo")
    fun getAllPhotos(): Flow<List<Photo>>

    @Query("SELECT * FROM Photo WHERE uid = :uid")
    fun getPhoto(uid: Int): Photo

}