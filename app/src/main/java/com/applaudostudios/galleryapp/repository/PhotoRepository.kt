package com.applaudostudios.galleryapp.repository

import com.applaudostudios.galleryapp.dao.PhotoDao
import com.applaudostudios.galleryapp.model.Photo
import com.applaudostudios.galleryapp.webservice.PhotoService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class PhotoRepository(private val photoDao: PhotoDao) {

    val allPhotos: Flow<List<Photo>> = photoDao.getAllPhotos()

    suspend fun refreshPhotos() {
        withContext(Dispatchers.IO) {

            val photos = PhotoService.getAllPhotos()
            photos?.let {
                photoDao.deleteAll()
                photoDao.insertAll(photos)
            }

        }
    }

    suspend fun getPhoto(id: Int) = withContext(Dispatchers.IO) { photoDao.getPhoto(id) }


}