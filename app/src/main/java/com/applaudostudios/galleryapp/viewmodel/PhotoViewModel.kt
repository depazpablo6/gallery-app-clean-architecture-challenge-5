package com.applaudostudios.galleryapp.viewmodel

import androidx.lifecycle.*
import com.applaudostudios.galleryapp.model.Photo
import com.applaudostudios.galleryapp.repository.PhotoRepository
import kotlinx.coroutines.launch

class PhotoViewModel(
    private val photoRepository: PhotoRepository
) : ViewModel() {

    val allPhotos: LiveData<List<Photo>> = photoRepository.allPhotos.asLiveData()
    val photo = MutableLiveData<Photo>()

    fun insertPhotos() = viewModelScope.launch {
        photoRepository.refreshPhotos()
    }

    fun getPhoto(id: Int) = viewModelScope.launch { photo.value = photoRepository.getPhoto(id) }

}