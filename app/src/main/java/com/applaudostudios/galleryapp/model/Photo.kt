package com.applaudostudios.galleryapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Photo(
    @field:SerializedName("id") @PrimaryKey val uid: Int,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("url") val url: String,
    @field:SerializedName("thumbnailUrl") val thumbnailUrl: String,
    @field:SerializedName("albumId") val albumId: Int
)