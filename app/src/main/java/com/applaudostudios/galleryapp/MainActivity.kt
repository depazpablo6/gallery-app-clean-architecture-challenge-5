package com.applaudostudios.galleryapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.galleryapp.adapter.PhotoAdapter
import com.applaudostudios.galleryapp.app.GalleryApplication
import com.applaudostudios.galleryapp.databinding.ActivityMainBinding
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var photoAdapter: PhotoAdapter

    private val photoViewModel: PhotoViewModel by viewModels {
        PhotoViewModelFactory((application as GalleryApplication).repository)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        photoViewModel.insertPhotos()
        createActivity()
        val postRecycler = binding.photoRecycler
        postRecycler.adapter = photoAdapter
        postRecycler.layoutManager = LinearLayoutManager(this)

        photoViewModel.allPhotos.observe(this, {
            it?.let {
                photoAdapter.submitList(it)
            }
        })


    }

    private fun createActivity() {
        photoAdapter = PhotoAdapter {
            val intent = Intent(this, PhotoDetailActivity::class.java).apply {
                putExtra(PhotoDetailActivity.PHOTO_ID, it)
            }
            startActivity(intent)
        }
    }

}