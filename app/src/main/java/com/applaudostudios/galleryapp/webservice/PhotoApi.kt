package com.applaudostudios.galleryapp.webservice

import com.applaudostudios.galleryapp.model.Photo
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoApi {

    @GET("/photos?")
    suspend fun getAllPhotos(@Query("albumId") albumId: Int): List<Photo>

}