package com.applaudostudios.galleryapp.webservice

import com.applaudostudios.galleryapp.model.Photo
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.UnknownHostException

object PhotoService {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val photoRequest = retrofit.create(PhotoApi::class.java)

    suspend fun getAllPhotos(): List<Photo>? {
        return try {
            photoRequest.getAllPhotos(6)
        } catch (e: UnknownHostException) {
            null
        }
    }

}