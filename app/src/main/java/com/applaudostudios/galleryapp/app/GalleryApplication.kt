package com.applaudostudios.galleryapp.app

import android.app.Application
import com.applaudostudios.galleryapp.database.GalleryDatabase
import com.applaudostudios.galleryapp.repository.PhotoRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class GalleryApplication : Application() {

    private val applicationScope = CoroutineScope(SupervisorJob())
    private val database by lazy { GalleryDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { PhotoRepository(database.photoDao()) }

}