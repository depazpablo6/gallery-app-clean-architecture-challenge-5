package com.applaudostudios.galleryapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.applaudostudios.galleryapp.dao.PhotoDao
import com.applaudostudios.galleryapp.model.Photo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Database(entities = [Photo::class], version = 1, exportSchema = false)
abstract class GalleryDatabase : RoomDatabase() {

    abstract fun photoDao(): PhotoDao

    private class GalleryDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let {
                scope.launch {
                    val photoDao = it.photoDao()
                    photoDao.deleteAll()
                }
            }
        }

    }


    companion object {

        @Volatile
        private var INSTANCE: GalleryDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope) =
            INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    GalleryDatabase::class.java,
                    "gallery_database"
                ).addCallback(GalleryDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }


    }


}